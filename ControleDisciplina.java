import java.util.ArrayList;

public class ControleDisciplina{

//Atributos

private ArrayList<Disciplina> listaDisciplinas;

//Construtor
	public ControleDisciplina(){
		listaDisciplinas = new ArrayList<Disciplina>();
	}	

//Metodos
	public String adicionarDisciplina(Disciplina umaDisciplina){
		String mensagem = "Disciplina adicionada com sucesso";
		listaDisciplinas.add(umaDisciplina);
		return mensagem;
	}
	
	public String removerDisciplina(Disciplina umaDisciplina){
		String mensagem = "Disciplina removida com sucesso";
		listaDisciplinas.remove(umaDisciplina);
		return mensagem;
	}

	public void exibirDisciplinas(){
		for(Disciplina umaDisciplina: listaDisciplinas){
			System.out.println(umaDisciplina.getNomeDisciplina());
		}
	}
	
	public Disciplina pesquisarNome(String umNome){
		for(Disciplina umaDisciplina: listaDisciplinas){
			if(umaDisciplina.getNomeDisciplina().equalsIgnoreCase(umNome)){
				return umaDisciplina;
			}	
		}
		return null;
	}	
}		
