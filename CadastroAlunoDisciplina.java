import java.io.*;

public class CadastroAlunoDisciplina{
	public static void main(String[] args) throws IOException{

	// burocracia
	InputStream entradaSistema = System.in;
	InputStreamReader leitor = new InputStreamReader(entradaSistema);
	BufferedReader leitorEntrada = new BufferedReader(leitor);
	String entradaTeclado;

	//declarando variaveis
	String umNome;
	String umCodigo;
	char menu = 'a';
	//instanciando objetos

	ControleAluno umControleAluno = new ControleAluno();
	ControleDisciplina umControleDisciplina = new ControleDisciplina();
	Aluno umAluno = new Aluno();
	Disciplina umaDisciplina = new Disciplina();

		// interagindo
		do{

			System.out.println("=======MENU=======");
			System.out.println("1- Adicionar Disciplina");
			System.out.println("2- Remover Disciplina");
			System.out.println("3- Listar Disciplinas");
			System.out.println("4- Adicionar aluno");
			System.out.println("5- Remover aluno");
			System.out.println("6- Listar alunos");
			System.out.println("b- Sair");	
			System.out.println("Digite a opção desejada: ");
	
			entradaTeclado = leitorEntrada.readLine();
			menu = entradaTeclado.charAt(0);
			
			while (menu != 'b' && menu != '1'&& menu != '2'&& menu != '3'&& menu != '4'&& menu != '5'&& menu != '6' ){
				System.out.println("O valor inserido é invalido =( Tente novamente: ");	
				entradaTeclado = leitorEntrada.readLine();
				menu = entradaTeclado.charAt(0);
	         	   }
	
				if(menu == '1'){
					System.out.println("Insira o nome da dicilpina: ");
					entradaTeclado = leitorEntrada.readLine();
					umNome = entradaTeclado;
					System.out.println("Insira o codigo da disciplina:");
					entradaTeclado = leitorEntrada.readLine();
					umCodigo = entradaTeclado;
					umaDisciplina = new Disciplina(umNome, umCodigo);
					String mensagem = umControleDisciplina.adicionarDisciplina(umaDisciplina);
					System.out.println(mensagem);
				}

				else if(menu == '2'){
					umControleDisciplina.exibirDisciplinas();	
					System.out.println("Insira o nome da disciplina a ser removida: ");
					entradaTeclado = leitorEntrada.readLine();
					umNome = entradaTeclado;
                    			umaDisciplina = umControleDisciplina.pesquisarNome(umNome);
                    			umControleDisciplina.removerDisciplina(umaDisciplina);
					String mensagem = umControleDisciplina.removerDisciplina(umaDisciplina);
					System.out.println(mensagem);
				}

				else if(menu == '3'){
					umControleDisciplina.exibirDisciplinas();
				}

				else if(menu == '4'){
					System.out.println("Insira o nome do aluno: ");
					entradaTeclado = leitorEntrada.readLine();
					umNome = entradaTeclado;
					System.out.println("Insira a matricula do aluno:");
					entradaTeclado = leitorEntrada.readLine();
					umCodigo = entradaTeclado;
					umAluno = new Aluno(umNome, umCodigo);
					String mensagem = umControleAluno.adicionar(umAluno);
					System.out.println(mensagem);
 	  			}

				else if(menu == '5'){
					umControleAluno.exibirAlunos();	
					System.out.println("Insira o nome do aluno a ser removido: ");
					entradaTeclado = leitorEntrada.readLine();
					umNome = entradaTeclado;
                    			umAluno = umControleAluno.pesquisarNome(umNome);
                    			umControleAluno.remover(umAluno);
					String mensagem = umControleAluno.remover(umAluno);
					System.out.println(mensagem);
				}
				
				else if(menu == '6'){
					umControleAluno.exibirAlunos();	
				}
				
    		}while (menu != 'b');

	}
}

