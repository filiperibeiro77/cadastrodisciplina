import java.util.ArrayList;
public class ControleAluno{
//Atributos
	private ArrayList<Aluno> listaAlunos;
	
//construtor
	public ControleAluno(){
	listaAlunos = new ArrayList<Aluno>();
	}
//metodos

	public String adicionar(Aluno umAluno){
		String mensagem = "Aluno adicionado com sucesso!!";
		listaAlunos.add(umAluno);
		return mensagem;
	}
	public String remover(Aluno umAluno){
		String mensagem = "Aluno removido com sucesso";
		listaAlunos.remove(umAluno);
		return mensagem;
	}
	public Aluno pesquisarNome(String umNome){
		for(Aluno umAluno: listaAlunos){
			if(umAluno.getNome().equalsIgnoreCase(umNome)){
				return umAluno;
			}
		}
		return null;
	}
	public void exibirAlunos(){
		for (Aluno umAluno:listaAlunos){
			System.out.println(umAluno.getNome());
		}
	}
}
